<Query Kind="Program" />

public const string filename = "D:\\Temp\\GCode\\CCR10S_Top_H20_Slots+Disp+Ant.gcode";
public string outputFile = "D:\\Temp\\GCode\\output.gcode";

void Main()
{
	var reg = new Regex("^G0 F\\d*\\.?\\d* X\\d*\\.?\\d* Y\\d*\\.?\\d* Z(\\d*\\.?\\d*)$");
	
	using (StreamWriter sw = File.AppendText(outputFile))
	{
		using (var fileStream = File.OpenRead(filename))
		{
			using (var reader = new StreamReader(fileStream, Encoding.UTF8, true, 128))
			{
				string line;
				while ((line = reader.ReadLine()) != null)
		    	{
					var match = reg.Match(line);
					
		      		if (match.Success)
					{
						var zHeight = Convert.ToDecimal(match.Groups[1].Value) - 0.2m;
						var newLine = Regex.Replace(line, "Z(\\d*\\.?\\d*)", $"Z{zHeight}");
						Console.WriteLine(newLine);
						sw.WriteLine(newLine);
					}
					else
					{
						sw.WriteLine(line);
					}
		    	}
			}
		}
	}
}

// You can define other methods, fields, classes and namespaces here
