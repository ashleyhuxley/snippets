import spidev
import RPi.GPIO as GPIO
import time

# Pin assignments
DC_PIN = 25    # Data/Command pin
RST_PIN = 27   # Reset pin
CS_PIN = 7     # Chip Select (CE0)

# Initialize GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(DC_PIN, GPIO.OUT)
GPIO.setup(RST_PIN, GPIO.OUT)

# Initialize SPI
spi = spidev.SpiDev()
spi.open(0, 0)  # SPI0, CE0
spi.max_speed_hz = 40000000  # Adjust as per the display's max speed

def write_command(cmd):
    GPIO.output(DC_PIN, GPIO.LOW)  # Command mode
    GPIO.output(CS_PIN, GPIO.LOW)
    spi.writebytes([cmd])
    GPIO.output(CS_PIN, GPIO.HIGH)

def write_data(data):
    GPIO.output(DC_PIN, GPIO.HIGH)  # Data mode
    GPIO.output(CS_PIN, GPIO.LOW)
    if isinstance(data, list):
        spi.writebytes(data)
    else:
        spi.writebytes([data])
    GPIO.output(CS_PIN, GPIO.HIGH)
        
def soft_reset():
    write_command(0x01)

def initialize_display():
    GPIO.output(RST_PIN, GPIO.LOW)
    time.sleep(0.1)
    GPIO.output(RST_PIN, GPIO.HIGH)
    time.sleep(0.1)

    set_gamma_control()
    set_power_control()
    set_vcom_control()
    set_memory_access_control()
    set_interface_pixel_format()
    set_interface_mode_control()
    set_frame_rate_control()
    set_display_inversion_control()
    set_display_function_control()
    set_entry_mode()
    set_adjust_control_3()
    set_sleep_out()
    set_display_on()
    
def set_vcom_control():
    write_command(0xC5)
    write_data([0x00, 0x40, 0x00, 0x40])
    
def set_memory_access_control():
    write_command(0x36)
    write_data([0x48])
    
def set_interface_pixel_format():
    write_command(0x3A)
    write_data([0x66])
    
def set_interface_mode_control():
    write_command(0xB0)
    write_data([0x00])
    
def set_frame_rate_control():
    write_command(0xB1)
    write_data([0xA0, 0x11])
    
def set_display_inversion_control():
    write_command(0xB4)
    write_data([0x02])
    
def set_display_function_control():
    write_command(0xB6)
    write_data([0x02, 0x02, 0x3b])
    
def set_entry_mode():
    write_command(0xB7)
    write_data([0x06])
    
def set_adjust_control_3():
    write_command(0xF7)
    write_data([0xA9, 0x51, 0x2C, 0x82b])
    
def set_sleep_out():
    write_command(0x11)
    write_data([0x80, 0x78b])
    
def set_display_on():
    write_command(0x29)
    write_data([0x80, 0x78b])

def set_power_control():
    write_command(0xC0)
    write_data([0x0E, 0x0E])
    
    write_command(0xC1)
    write_data([0x44])

def set_gamma_control():
    # Positive Gamma Control
    write_command(0xE0)
    write_data([0x00, 0x07, 0x0C, 0x05, 0x13, 0x09, 0x36, 0xAA, 0x46, 0x09, 0x10, 0x0D, 0x1A, 0x1E, 0x1F])

    # Negative Gamma Control
    write_command(0xE1)
    write_data([0x00, 0x20, 0x23, 0x04, 0x10, 0x06, 0x37, 0x56, 0x49, 0x04, 0x0C, 0x0A, 0x33, 0x37, 0x0F])

# Draw a gradient
def draw_gradient():
    write_command(0x2A)  # Column Address Set
    write_data([0x00, 0x00, 0x01, 0x3F])  # 320 columns (0x13F)
    
    write_command(0x2B)  # Page Address Set
    write_data([0x00, 0x00, 0x01, 0xDF])  # 480 rows (0x1DF)
    
    write_command(0x2C)  # Memory Write
    for y in range(480):
        for x in range(320):
            color = [y / 2, 0, 0]
            # color = [y % 256, x % 256, (x + y) % 256]  # RGB gradient
            write_data(color)

try:
    initialize_display()
    draw_gradient()
    print("Gradient displayed successfully!")
except Exception as e:
    print(f"Error: {e}")
finally:
    spi.close()
    GPIO.cleanup()